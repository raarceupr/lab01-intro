Objetivos:
==========

En la experiencia de laboratorio del día de hoy te explicaremos cómo
trabajaremos en las próximas reuniones, la manera de acceder los
archivos que se usarán y cómo entregar los trabajos. También aprenderás
a manejar los elementos básicos de Qt, la plataforma que nos permitirá
desarrollar y ejecutar proyectos en C++.

Servicios y programados que utilizaremos:
=========================================

Bitbucket y Git
---------------

*Bitbucket* es un repositorio o depósito de archivos digitales al que se
puede acceder en línea y que permite trabajar proyectos en grupo de
manera ordenada y simple. Los archivos del Laboratorio de Introducción a
la Programación estarán almacenados en este lugar y se podrán bajar a
las computadoras personales utilizando *Git*.

*Git* es un programado de código abierto que permite manejar archivos de
programas que se desarrollan en grupo. Puedes obtener este programado en
<http://git-scm.com/>. Los archivos de las experiencias de laboratorio
serán guardados en cada una de las laptops del salón usando el comando
`git clone url` desde el terminal de cada computadora. Para las
experiencias de laboratorio que sea necesario, actualizaremos estos
archivos utilizando el comando `git pull` desde el terminal, en el
directorio que contiene los archivos originales en cada laptop.

Terminal y Linux
---------------

Para utilizar Git en OS X debemos usar comandos de línea en la pantalla de *terminal*. Los comandos que utilizaremos son comandos del sistema operativo *Unix*. Unix distingue entre letras mayúsculas y minúsculas. Algunos comandos básicos de Unix son:

<center>

|    **Comando**     |                      **Acción**                     |
| ------------------ | --------------------------------------------------- |
| ls                 | muestra lista de los archivos en el directorio      |
| mv nombre1 nombre2 | mueve contenido de nombre1 a archivo nombre2        |
| cp nombre1 nombre2 | copia contenido de nombre1 a archivo nombre2        |
| rm nombre          | borra archivo                                       |
| mkdir nombre       | crea directorio nombre dentro del directorio actual |
| cd ..              | cambia al directorio anterior                       |
| cd ~               | cambia al directorio hogar                          |
| cd nombre          | cambia al directorio nombre (dentro del actual)     |
| pwd                | "path name" del "working" "directory"               |
| flecha hacia arriba| repite comando anterior                             |

</center>            

Qt
--

Qt es una aplicación para programación que es utilizada por
desarrolladores que usan el lenguaje de programación C++. Este ambiente
funciona en, y puede crear versiones de, las aplicaciones para distintas
plataformas (desktop, plataformas móbiles y otras). Qt contiene un
ambiente de desarrollo integrado (IDE), llamado *Qt Creator*. Desde ese
ambiente se puede programar y crear interfaces gráficas utilizando la
opción de diseño que contiene Qt. Te instamos a que instales Qt en tu
computadora personal y explores las otras opciones que esta aplicación
provee.

### Como obtener Qt

La aplicación Qt está instalada en cada una de las laptops del
laboratorio. Busca el ícono de Qt y haz doble click para comenzar la
aplicación.

En la página del proyecto Qt, <http://qt-project.org/>, puedes encontrar
más información sobre Qt y desde ella puedes bajar el programa para que
lo instales en tu computadora.

### Usando Qt

Qt es una aplicación usada por desarrolladores profesionales y tiene
muchísimas posibilidades y opciones. En esta experiencia de laboratorio
veremos como utilizar las opciones básicas que necesitaremos durante el
semestre.

Para el laboratorio mayormente utilizaremos la parte de editar programas
en C++ pero también existe una opción para diseñar interfaces gráficos.
Esta opción fue utilizada para incorporar el código que presenta los
interfaces gráficos que se presentan en las experiencias de laboratorio.
El aprender a utilizar esta opción no es parte de este curso pero puedes
aprender a utilizarla por tu cuenta. En [este
enlace](https://docs.google.com/file/d/0B_6PcmpWnkFBOXJxVDlUNEVfOFk/edit "Taller Qt")
puedes encontrar una presentación, preparada por el estudiante Jonathan
Vélez, que muestra aspectos básicos de cómo utilizar la opción de diseño
de interfaces gráficos.

### Proyectos

Cada proyecto en C++ se compone de varios tipos de archivos. En Qt
tendrás archivos del tipo *fuente (source), encabezados (header) y
formulario (form)*.

-   **Archivos "sources":** Estos archivos tienen extensión `.cpp` (C plus
    plus) y contienen el código en C++ de tu programa. Entre estos
    archivos encontrarás el *main.cpp*; este es el archivo que buscará
    el preprocesador y es donde comienza tu programa. Otro de los
    archivos tipo "source" que encontraras en proyectos creados en Qt es
    el archivo *mainwindow.cpp*; este archivo lo crea Qt y contiene el
    código asociado a la ventana principal diseñada con la opción de
    design (por ejemplo las funciones que aparecen bajo "Private
    slots").

-   **Archivos "headers":** Estos archivos tienen extensión `.h` y
    contienen declaraciones de las funciones que son utilizadas en el programa. Durante el pre-procesamiento de cada programa, la
    instrucción `#include<nombre.h>` incluye el contenido del archivo
    llamado "nombre.h" en el codigo del archivo `.cpp` que contiene esa
    instrucción.

-   **Archivos "forms":** Estos archivos tienen extension `.ui` (user
    interface) y contienen los formularios creados con la opción de
    diseño. Un archivo que encontrarás en proyectos creados en Qt es el
    archivo *mainwindow.ui*; este archivo lo crea Qt y contiene el
    diseño de la ventana principal del programa.

#### Comenzar proyecto nuevo (Ejercicio 1)

Para comenzar un proyecto en C++, marca el botón de "New Project" o ve
al menú principal de Qt y en "File" selecciona "New File or Project".
Saldrá una ventana como la que sigue

<div align='center'><img src="http://i.imgur.com/NEn9LWm.png" width="400" height="300" alt="New Project" /></div>



ahí selecciona "Applications, Qt Wigets Application" y marca "Choose".
Guarda el archivo. Anota el directorio en donde lo guardaste para que luego puedas copiarlo; recuerda que las computadoras del laboratorio las usa mucha gente y pueden borrar los archivos.
Marca "Continue" en las pantallas de "Kit
Selection", "Class Information", y marca "Done" en la ventana de
"Project Management".

El proceso de arriba crea los archivos básicos del proyecto, incluyendo
uno con extensión `.pro` que es con el que luego podrás cargar los
archivos del proyecto en Qt. Al finalizar el proceso deberá aparecer una
pantalla con el "mainwindow.cpp". En el directorio de "Projects"
selecciona "main.cpp" de modo que obtengas una pantalla parecida a la
siguiente:

<div align='center'><img src="http://i.imgur.com/3og2lFR.png" width="450" height="325" alt="Main.cpp" /></div>



Borra el contenido de "main.cpp" y escribe:


    #include <iostream>
    using namespace std;

    int main()
    {
        cout << endl << "Me gusta el laboratorio de programación." << endl;
        return 0;
    }


Esto te dará el esqueleto básico de un programa en C++. Marca el botón verde del menú de la izquierda para compilar y ejecutar el programa. Saldrá una ventana que te ofrece la opción de guardar los cambios. Marca "Save all". Al ejecutar, el programa debe desplegar "Me gusta el laboratorio de programación." en la pantalla de "Application Output".

Durante el proceso de compilación y ejecución Qt crea varios archivos que debemos borrar luego de terminar con el programa. Para hacer esto, en la opción "Build" del menú de Qt, selecciona "Clean All".


#### Bajar archivos de Bitbucket (Ejercicio 2)

Los archivos para esta experiencia de laboratorio están guardados en Bitbucket. Para bajar estos archivos a tu computadora ve a la pantalla de terminal y escribe el comando `git clone https://bitbucket.org/eip-uprrp/Lab01-Intro`.


#### Abrir proyecto ya creado compilar y ejecutar (Ejercicio 3) 

Vamos a practicar como compilar, corregir errores y ejecutar un programa
usando Qt. En el directorio "Practica", haz doble "click" en el archivo
"Practica.pro" para cargar el proyecto a Qt. En la ventana que aparece
marca "Configure Project".

Qt te permite compilar y ejecutar el programa marcando la flecha verde
que aparece en la columna de la izquierda. Presiona la flecha y nota que
obtienes una ventana de "Issues" que ocurrieron al compilar. La lista
que aparece te muestra información que te permitirá encontrar y corregir
los errores. Selecciona el archivo "main.cpp" en el directorio de
"Sources" para que puedas encontrar y corregir los errores. Una vez
corrijas todos los errores, el programa debe abrir la pantalla
"Application Output" y desplegar "Salida: 1".

#### Entrega de trabajos (Ejercicio 4)

Durante cada experiencia de laboratorio cada pareja deberá entregar algunos resultados de su trabajo. Estas entregas se harán en la sección "Entregas Lab". La manera más fácil para  hacer las entregas es abrir el enlace de ["Entregas Lab"](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6456)
en una pestaña nueva del "browser"; esto se logra marcando la tecla de `command` y a la misma vez marcar [este enlace](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6456).

Hoy cada estudiante practicará una entrega individualmente. Abre el enlace de ["Entregas Lab"](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6456) en una pestaña nueva del "browser" y sigue las instrucciones.


